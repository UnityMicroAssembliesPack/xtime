﻿using UnityEngine;

public static class XTime
{
    public struct Timer {
        public void reset(float inTotalTime) {
            _isActive = true;
            _remainingTime = inTotalTime;
        }

        public bool isActive() {
            return _isActive;
        }

        public float getRemainingTime() {
            return _remainingTime;
        }

        public bool updateAndCheckIfFired(float inDeltaTime) {
            XUtils.check(_isActive);
            
            _remainingTime -= inDeltaTime;
            if (_remainingTime <= 0.0f) {
                _isActive = false;
                _remainingTime = 0.0f;
                return true;
            } else {
                return false;
            }
        }

        private bool _isActive;
        private float _remainingTime;
    }
}
